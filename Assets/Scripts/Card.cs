﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CreateAssetMenu(menuName ="Game/Card/New Card")]
[System.Serializable]
public class Card : ScriptableObject { 
    [SerializeField]
    [Tooltip("The name of the card")]
    public string Name;
    [SerializeField]
    [Tooltip("The card description")]
    public string Description;
    [SerializeField]
    [Tooltip("The art of the card")]
    public Sprite CardArt;
    [Header("--------------------")]
    [Space(10)]
    [SerializeField]
    [Tooltip("The energy start of variation")]
    public float EnergyLow = 0;
    public bool IsEnergyVariation = false;
    [ConditionalHide("IsEnergyVariation")]
    [SerializeField]
    [Tooltip("The energy end of variation")]
    public float EnergyHigh = 0;
    [Space(10)]
    [Header("--------------------")]
    [SerializeField]
    [Tooltip("The hygiene start of variation")]
    public float HygieneLow = 0;
    public bool IsHygieneVariation = false;
    [ConditionalHide("IsHygieneVariation")]
    [SerializeField]
    [Tooltip("The hygiene end of variation")]
    public float HygieneHigh = 0;
    [Space(10)]
    [Header("--------------------")]
    [SerializeField]
    [Tooltip("The comfort start of variation")]
    public float ComfortLow = 0;
    public bool IsComfortVariation = false;
    [ConditionalHide("IsComfortVariation")]
    [SerializeField]
    [Tooltip("The comfort end of variation")]
    public float ComfortHigh = 0;
    [Header("--------------------")]
    [Space(10)]
    [SerializeField]
    public float Time = 0;
    [Space(10)]
    [SerializeField]
    public float Cost = 0;
}