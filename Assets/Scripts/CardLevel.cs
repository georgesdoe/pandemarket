﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum CardLevel
{
    Bronze = 0,
    Silver = 1,
    Gold = 2,
    Diamond = 3
}
