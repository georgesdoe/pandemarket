﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Game/Stages/New Stage")]
[System.Serializable]
public class Stage : ScriptableObject
{
    [SerializeField]
    public float TimeLimit = 0f;
    [SerializeField]
    public float TotalBudget = 0f;
    [SerializeField]
    public string StageName = "New Stage";
    [SerializeField]
    public List<Card> Cards;
}
