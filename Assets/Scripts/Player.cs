﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Game/Player/New Player")]
[System.Serializable]
public class Player : ScriptableObject
{
    public string Name = "";
    public int HighScore = 0;
    public float Energy = 0;
    public float Hygiene = 0;
    public float Comfort = 0;
}
