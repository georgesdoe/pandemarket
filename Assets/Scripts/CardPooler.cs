﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardPooler : MonoBehaviour
{
    protected List<GameObject> active = new List<GameObject>();

    protected Stack<GameObject> inactive = new Stack<GameObject>();

    public GameObject Spawn(GameObject prefab, Transform parent)
    {
        if (inactive.Count > 0)
        {
            var instance = inactive.Pop();
            instance.SetActive(true);
            instance.transform.SetParent(parent);
            active.Add(instance);
            return instance;
        }
        else
        {
            var instance = Instantiate(prefab, parent);
            active.Add(instance);
            return instance;
        }
    }

    public void Despawn(GameObject instance)
    {
        active.Remove(instance);
        instance.SetActive(false);
        inactive.Push(instance);
    }
}
