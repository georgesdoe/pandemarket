﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CardController : MonoBehaviour
{
    public Image frameReference;
    public Image picReference;

    public Text titleReference, descriptionReference, priceReference;

    private CardSelection selection;

    public Sprite bronzeFrame, silverFrame, goldFrame, diamondFrame;

    public void Select()
    {
        GameManager.Instance.AddCard(selection);
    }

    public void SetCard(Card card, CardLevel level)
    {
        selection = new CardSelection(card,level);

        picReference.sprite = card.CardArt;
        titleReference.text = card.Name;
        descriptionReference.text = card.Description;
        priceReference.text = selection.Cost.ToString("C");

        switch (level)
        {
            case CardLevel.Bronze:
                frameReference.sprite = bronzeFrame;
                break;
            case CardLevel.Silver:
                frameReference.sprite = silverFrame;
                break;
            case CardLevel.Gold:
                frameReference.sprite = goldFrame;
                break;
            case CardLevel.Diamond:
                frameReference.sprite = diamondFrame;
                break;
            default:
                Debug.LogError("Unrecognized card level " + level);
                break;
        }
    }
}
