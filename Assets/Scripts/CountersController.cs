﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CountersController : MonoBehaviour
{

    public Image energyRef, hygieneRef, comfortRef;

    private float energy, hygiene, comfort;

    private bool isTicking = false;

    public float TickTime = 0.5f;

    public float EnergyRate = 0.01f, HygieneRate=0.05f, ComfortRate=0.04f;

    private float lastTick = 0f;

    public void Begin()
    {
        energy = hygiene = comfort = 1;
        isTicking = true;
    }

    public void Stop()
    {
        isTicking = false;
    }

    void FixedUpdate()
    {
        if (!isTicking)
        {
            return;
        }

        if (lastTick >= TickTime)
        {
            lastTick -= TickTime;
            Tick();
        }
        else
        {
            lastTick += Time.deltaTime;
        }
    }

    void Tick()
    {
        ModifyEnergy(-EnergyRate);
        ModifyHygiene(-HygieneRate);
        ModifyComfort(-ComfortRate);
        if (energy == 0f || hygiene == 0f || comfort == 0f)
        {
            GameManager.Instance.GameOver();
        }
    }

    public void ModifyEnergy(float amount)
    {
        energy = Mathf.Clamp01(energy + amount);
        energyRef.fillAmount = energy;
    }

    public void ModifyHygiene(float amount)
    {
        hygiene = Mathf.Clamp01(hygiene + amount);
        hygieneRef.fillAmount = hygiene;
    }

    public void ModifyComfort(float amount)
    {
        comfort = Mathf.Clamp01(comfort + amount);
        comfortRef.fillAmount = comfort;
    }
}
