﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BudgetController : MonoBehaviour
{
    public Text textRef;

    protected float currentBudget = 0f;

    public void SetBudget(float amount)
    {
        currentBudget = amount;
        updateText();
    }

    public void SpendBudget(float amount)
    {
        currentBudget -= amount;
        updateText();
    }

    protected void updateText()
    {
        textRef.text = "Budget: " + currentBudget.ToString("C");
    }

    public bool CanSpend(float amount)
    {
        return currentBudget - amount >= 0f;
    }
}
