﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimerController : MonoBehaviour
{
    public Image timerImage;

    public float tickAmount = 1f;

    protected float totalSeconds;
    protected float remainingSeconds;

    public void Restart()
    {
        StopCoroutine("Tick");
        remainingSeconds = totalSeconds;
        StartCoroutine("Tick");
    }

    public void Begin(float seconds)
    {
        totalSeconds = remainingSeconds = seconds;
        StartCoroutine("Tick");
    }

    public IEnumerator Tick()
    {
        while(remainingSeconds > 0)
        {
            remainingSeconds -= tickAmount;
            timerImage.fillAmount = remainingSeconds / totalSeconds;

            yield return new WaitForSeconds(tickAmount);
        }

        GameManager.Instance.GameOver();

        yield return null;
    }
}
