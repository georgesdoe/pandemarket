﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public Stage stage;

    public Player player;

    public GameObject cardPrefab;

    public Transform cardSpawnPoint;

    public GameObject gameOverPanel;

    public TimerController timer;

    public BudgetController budget;

    public CountersController counters;

    public CardPooler pooler;

    private List<CardSelection> selectedCards = new List<CardSelection>();

    private List<GameObject> currentCards = new List<GameObject>();

    public static GameManager Instance { get; private set; }

    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(gameObject);
            return;
        }

        Instance = this;
        DontDestroyOnLoad(gameObject);
    }

    void Start()
    {
        budget.SetBudget(stage.TotalBudget);
        timer.Begin(stage.TimeLimit);
        counters.Begin();
        DrawCards();
    }

    void DrawCards()
    {
        for (int i = 0; i < 4; i++)
        {
            int cardIndex = Random.Range(0, stage.Cards.Count);
            int frame = Random.Range(0, 4);

            SpawnCard(cardIndex, (CardLevel)frame);
        }
    }

    void SpawnCard(int index, CardLevel level)
    {
        var card = pooler.Spawn(cardPrefab,cardSpawnPoint);
        var controller = card.GetComponent<CardController>();
        controller.SetCard(stage.Cards[index], level);
        currentCards.Add(card);
    }

    public void AddCard(CardSelection selection)
    {
        if (!budget.CanSpend(selection.Cost))
        {
            GameOver();
            return;
        }
        budget.SpendBudget(selection.Cost);
        selectedCards.Add(selection);

        counters.ModifyEnergy(selection.Energy);
        counters.ModifyHygiene(selection.Hygiene);
        counters.ModifyComfort(selection.Comfort);

        RedrawCards();
    }

    protected void RedrawCards()
    {
        foreach (var item in currentCards)
        {
            pooler.Despawn(item);
        }
        currentCards.Clear();
        DrawCards();
    }

    public void GameOver()
    {
        gameOverPanel.SetActive(true);
        counters.Stop();
    }

    public void Restart()
    {
        budget.SetBudget(stage.TotalBudget);
        selectedCards.Clear();
        RedrawCards();
        timer.Restart();
        counters.Begin();
        gameOverPanel.SetActive(false);
    }
}
