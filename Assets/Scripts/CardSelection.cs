﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardSelection
{
    public Card Card { get; private set; }

    public CardLevel Level {get; private set;}

    public float Cost { get; private set; }

    public float Hygiene { get; private set; }

    public float Energy { get; private set; }

    public float Comfort { get; private set; }

    public CardSelection(Card card, CardLevel level)
    {
        Card = card;
        Level = level;
        Cost = CalculateCost();
        Hygiene = CalculateHygiene();
        Comfort = CalculateComfort();
        Energy = CalculateEnergy();
    }

    protected float CalculateCost()
    {
        float baseValue = Card.Cost;
        return baseValue * GetLevelMultiplier();
    }

    protected float CalculateHygiene()
    {
        float baseValue = Card.IsHygieneVariation ?
            Random.Range(Card.HygieneLow, Card.HygieneHigh) : Card.HygieneLow;
        return baseValue * GetLevelMultiplier();
    }

    protected float CalculateEnergy()
    {
        float baseValue = Card.IsEnergyVariation ?
            Random.Range(Card.EnergyLow, Card.EnergyHigh) : Card.EnergyLow;
        return baseValue * GetLevelMultiplier();
    }

    protected float CalculateComfort()
    {
        float baseValue = Card.IsComfortVariation ?
            Random.Range(Card.ComfortLow, Card.ComfortHigh) : Card.ComfortLow;
        return baseValue * GetLevelMultiplier();
    }

    protected float GetLevelMultiplier()
    {
        switch (Level)
        {
            case CardLevel.Bronze:
                return 1f;
            case CardLevel.Silver:
                return 1.5f;
            case CardLevel.Gold:
                return 2f;
            case CardLevel.Diamond:
                return 3.5f;
            default:
                Debug.LogError("Unrecognized card level " + Level);
            break;
        }
        return 0f;
    }
}
